#region Report-Service
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualBasic.FileIO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;


namespace ReportService
{
    #region Orrders-Models

    #region orrders
    public class orrders
    {


        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]

        public string _id { get; set; }

        public string date { get; set; }

        public DateTime createdAt { get; set; }

        public DateTime updatedAt { get; set; }
        public List<Orders> orders { get; set; }


    }
    #endregion

    #region orders
    public class Orders
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public List<ShopTypeCategories> shopTypeCategories { get; set; }
    }
    #endregion

    #region Shop-Type-category
    public class ShopTypeCategories
    {
        public int shopTypeId { get; set; }
        public string shopTypeName { get; set; }
        public List<StoreOrders> storeOrders { get; set; }
    }
    #endregion

    #region Store-Orders
    public class StoreOrders
    {
        public int storeId { get; set; }
        public string storeName { get; set; }
        public List<CustomerOrders> customerOrders { get; set; }
    }
    #endregion

    #region Customer-Orders
    public class CustomerOrders
    {
        public DateTime orderTimeStamp { get; set; }
        public int customerId { get; set; }
        public int personId { get; set; }
        public string orderId { get; set; }
        public int orderAmount { get; set; }
        public string orderStatus { get; set; }
    }
    #endregion
    #endregion

    #region OrderResponse-Model
    public class OrderResponse
    {
        public string status { get; set; }
        public object data { get; set; }
        public string message { get; set; }
    }
    #endregion

    #region Values-Model
    public class Values
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string date { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public int shopTypeId { get; set; }
        public string shopTypeName { get; set; }
        public int storeId { get; set; }
        public string storeName { get; set; }
        public DateTime orderTimeStamp { get; set; }
        public int customerId { get; set; }
        public int personId { get; set; }
        public string orderId { get; set; }
        public int orderAmount { get; set; }
        public string orderStatus { get; set; }
        public string orderType { get; set; }
        public string orderStatusDes { get; set; }
        public string orderNumber { get; set; }
        public string orderuri { get; set; }
        public string mailurl { get; set; }
        public string path { get; set; }
    }
    #endregion

    #region Email-Models
    public class Email
    {
        public string to { get; set; }
        public string cc { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public IFormFile[] attachment { get; set; }

    }
    #endregion

    
    #region Service
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly orrders _content;
        public readonly Values _values;
        public readonly Email _email;
        ArrayList nlist = new ArrayList();
        ArrayList orderCount = new ArrayList();
        ArrayList amount = new ArrayList();
        public DateTime _lastRun;
        private int sums;

        #region Constructor
        public Worker(ILogger<Worker> logger, IOptions<orrders> content, IOptions<Values> values, IOptions<Email> email)
        {
            _logger = logger;
            _content = content.Value;
            _values = values.Value;
            _email = email.Value;
        }
        #endregion

        #region Task-Start-Async
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Service Started: {time}", DateTimeOffset.Now);
            return base.StartAsync(cancellationToken);
        }
        #endregion

        #region Task-Stop-Async
        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Service Stopped: {time}", DateTimeOffset.Now);
            return base.StopAsync(cancellationToken);
        }
        #endregion

        #region Task-Execute-Async
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {                
                if (_lastRun < DateTime.Today)
                {
                    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                     await call();
                     await Mail();
                }                
                await Task.Delay(1000 * 10, stoppingToken);
            }
        }
        #endregion

        #region Call-Method
        private async Task<List<orrders>> call()
        {   var orderuri = Environment.GetEnvironmentVariable("ORDER_URI");
            
             var csvpath = Environment.GetEnvironmentVariable("CSV_PATH");
            
            List<orrders> res = new List<orrders>();
            using (var httpClient = new HttpClient())
            {
                #region Prepare Http payload for OrderReport Request

                //var TodayDate = DateTime.Now.ToString("yyyy-MM-dd");
                var TodayDate = "2022-04-06";
                var ddd = TodayDate;   
                Uri orderReportRqUri = new Uri(orderuri);              
                var payload = "{\"date\": \"" + ddd + "\"}";               
                HttpContent orderReportRq = new StringContent(payload, Encoding.UTF8, "application/json");                
                httpClient.DefaultRequestHeaders.Add("bucketToken", "22E97B9F-14D7-4637-91A9-4ED5D6E4A0FA");
                #endregion
                using (var response =  await httpClient.PostAsync(orderReportRqUri, orderReportRq))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                   // var result = JsonConvert.DeserializeObject<OrderResponse>(apiResponse);
                    string str = apiResponse.ToString();                    
                    string fpath = csvpath;
                    string fName = "Report.csv";
                    JObject json = JObject.Parse(str);
                    Values tm = new Values();
                    //var mydata = result.data;//_logger.LogInformation("Below is my data");
                    //_logger.LogInformation("=>" + apiResponse + "<=");
                    #region  Data
                    foreach (var a in json["data"])
                    {
                        foreach (var b in a["orders"]["shopTypeCategories"])
                        {
                            _values.shopTypeId = (int)b["shopTypeId"];
                            _values.shopTypeName = (string)b["shopTypeName"];
                            tm.shopTypeName = _values.shopTypeName;
                            tm.shopTypeId = _values.shopTypeId;                           
                            nlist.Add(tm.shopTypeName);
                            int cb = b["storeOrders"].Count();

                            foreach (var c in b["storeOrders"])
                            {
                                _values.storeId = (int)c["storeId"];
                                _values.storeName = (string)c["storeName"];
                                tm.storeId = _values.storeId;
                                tm.storeName = _values.storeName;
                                nlist.Add(tm.storeId);
                                nlist.Add(tm.storeName);
                                
                                foreach (var d in c["customerOrders"])
                                {
                                    _values.orderTimeStamp = (DateTime)d["orderTimeStamp"];
                                    _values.customerId = (int)d["customerId"];
                                    _values.orderId = (string)d["orderId"];
                                    _values.orderAmount = (int)d["orderAmount"];
                                    _values.orderStatus = (string)d["orderStatus"];
                                    _values.orderType = (string)d["orderType"];
                                    _values.orderStatusDes = (string)d["orderStatusDes"];
                                    _values.orderNumber = (string)d["orderNumber"];
                                    tm.orderId = _values.orderId;
                                    tm.orderAmount = _values.orderAmount;
                                    orderCount.Add(tm.orderId);
                                    amount.Add(tm.orderAmount);
                                }
                                int cn = orderCount.Count;
                                nlist.Add(cn);

                                int am = amount.Count;
                                for (int aa = 0;aa<am;aa++)
                                {
                                    sums += (int)amount[aa];
                                }
                                nlist.Add(sums);
                                sums = 0;
                                for (int cls = 0; cls < am; am--)
                                {
                                    amount.RemoveAt(cls);
                                }

                                #region Clearing-ArrayList-for-data-summing
                                int cz = orderCount.Count;                               
                                for (int cl = 0; cl < cz; cz--)
                                {
                                    orderCount.RemoveAt(cl);
                                }
                                #endregion                                
                                --cb;
                                if(cb == 0)
                                {
                                    break;
                                }
                                 nlist.Add("");
                            }
                        }
                    }
                    #endregion

                    #region Stream
                    using (StreamWriter sw = new StreamWriter(fpath + fName))
                    {
                        if (nlist.Count != 0)
                        {
                            sw.WriteLine("Shop Type,Store Id,Store Name,No of Orders, Amount Total");
                            int stride = nlist.Count / 5;
                            int i = 0;
                            for (int rows = 0; rows < stride; rows++)
                            {
                                string r = "";
                                for (int col = 0; col < 5; col++)
                                {
                                    r += nlist[i];
                                    if (col != 5)
                                    {
                                        r += ",";
                                    }
                                    i++;
                                };
                                sw.WriteLine(r);
                            }
                        }
                        else
                        {
                            sw.WriteLine("No Data for Today" + DateTime.Now);
                        }

                        
                        int strid = nlist.Count;
                        for (int cl = 0; cl < strid; strid--)
                        {
                            nlist.RemoveAt(cl);
                        }
                    }
                    #endregion
                }
            }
            return res;
        }
        #endregion
        
        #region Mail_Method
        private async Task<Email> Mail()
        {
            var mailuri = Environment.GetEnvironmentVariable("MAIL_URL");
            var csvpath = Environment.GetEnvironmentVariable("CSV_PATH");
            try
            {
                HttpClient httpClient = new HttpClient();
                Email email = new Email();
                email.to = _email.to;
                email.cc = _email.cc;
                email.subject = _email.subject;
                email.body = ("Dear Sir," +
                            "<br>" + " Please Find the attachements below" +
                            "<br>" + " " +
                            "<br>" + " " +
                            "<br>" + " " +
                            "<br>" + " " +
                            "<br>" + " " +
                            "<br>" + "Thanks & Regards" +
                            "<br>" + "Oritso" +
                            "<br>");
                email.attachment = _email.attachment;  
                string fileName = "Report.csv";
                var filePath = csvpath;                
                MultipartFormDataContent form = new MultipartFormDataContent();                
                form.Add(new StringContent(email.to), "to");                
                form.Add(new StringContent(email.cc), "cc");                
                form.Add(new StringContent(email.subject), "subject");                
                form.Add(new StringContent(email.body), "body");
                var p = Path.GetFullPath(filePath + fileName);
                byte[] readText = File.ReadAllBytes(p);                
                ByteArrayContent fileContent = new ByteArrayContent(readText);                
                fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = "attachment",
                    FileName = System.IO.Path.GetFileName(p)
                };
                form.Add(fileContent,"attachment","Report.csv");
                var httpClientHandler = new HttpClientHandler();                
                HttpResponseMessage response = await httpClient.PostAsync(mailuri, form);                
                response.EnsureSuccessStatusCode();
                httpClient.Dispose();
                if (response.IsSuccessStatusCode)
                {
                    _logger.LogInformation("<----------Mail Sended Successfully To----------> " + email.to);
                    _lastRun = DateTime.Today;
                }
                else
                {
                    _logger.LogInformation("Something went wrong !! Error = ", response.StatusCode);
                }
                return email;
            }
            catch (Exception ex)
            {
                Email email = new Email();
                Log.Fatal(ex, "There was a poblem starting the service");
                return email;
            }
        }
        #endregion
    }
    #endregion

}
#endregion