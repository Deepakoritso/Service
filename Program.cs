using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string logsDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs.txt");
            Log.Logger = new LoggerConfiguration()
              .MinimumLevel.Debug()
              .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
              .Enrich.FromLogContext()
              .WriteTo.File(logsDirectory)
              .CreateLogger();
            try
            {
                Log.Information("Starting from the Main Program.cs");
                Log.Information("Service is Running.........");
                CreateHostBuilder(args).Build();
                IHost host = CreateHostBuilder(args).Build();
                host.Run();
                return;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "There was a poblem starting the service");
                return;
            }
            finally
            {
                //Below code used for if any messages in a buffer  it will flush them all.
                Log.CloseAndFlush();
            }           
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration configuration = hostContext.Configuration;
                    AppSettings.Configuration = configuration;

                    //services.Configure<orrders>(configuration.GetSection("Configs"));
                    //services.Configure<Values>(configuration.GetSection("Config"));
                    services.Configure<Email>(configuration.GetSection("Emails"));

                    services.AddHostedService<Worker>();
                })
               .UseSerilog();
    }
}
